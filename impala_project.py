import pyodbc
import time
import pprint
import sys

impala_conn = "DSN=MdcloneApp"
log_path = "C:\\impala_migration\\migration_hierarchy_log.txt"

# return data from database
def execute_query(conn_string, query):
    try:
        conn = pyodbc.connect(conn_string, autocommit=True)
        curs = conn.cursor()
        curs.execute(query)
        curs.commit()
        return curs.fetchall()
    except:
        return sys.exc_info()

# write documentation to run script
def log(log_path,content):
    file_log = open(log_path, 'a+')
    file_log.write(time.strftime('%Y-%m-%d %H:%M:%S') + "   " + content + '\n')

# create new table partition with temp name
def partition(db_name,project_id):
    try:
        partition_query = """CREATE TABLE IF NOT EXISTS {db_name}.mdc_hierarchy_level_item_{project_id}_temp (
            hierarchy_level_item_id BIGINT,
            hd_0 STRING,
            hd_1 STRING,
            hd_2 STRING,
            hd_3 STRING,
            hd_4 STRING,
            hd_5 STRING,
            hd_6 STRING,
            hd_7 STRING,
            hd_8 STRING,
            hd_9 STRING,
            hd_10 STRING,
            hd_11 STRING,
            hd_12 STRING,
            hd_13 STRING,
            hd_14 STRING,
            hd_15 STRING,
            hd_16 STRING,
            hd_17 STRING,
            hd_18 STRING,
            hd_19 STRING
        ) PARTITIONED BY (hierarchy_root_id BIGINT) STORED AS PARQUET""".format(**locals())
        execute_query(impala_conn, partition_query)
        log(log_path, "the query " + partition_query + " return true")
    except:
        log(log_path, "the query " + partition_query + " return false with the error: " + str(sys.exc_info()))

# insert the old date from the orginal table to the new temp table as partition
def insert(db_name,project_id):
    try:
        insert_query = """INSERT OVERWRITE {db_name}.mdc_hierarchy_level_item_{project_id}_temp (
            hierarchy_level_item_id,
            hierarchy_root_id,
            hd_0,
            hd_1,
            hd_2,
            hd_3,
            hd_4,
            hd_5,
            hd_6,
            hd_7,
            hd_8,
            hd_9,
            hd_10,
            hd_11,
            hd_12,
            hd_13,
            hd_14,
            hd_15,
            hd_16,
            hd_17,
            hd_18,
            hd_19
        ) SELECT
            hierarchy_level_item_id,
            hierarchy_root_id,
            hd_0,
            hd_1,
            hd_2,
            hd_3,
            hd_4,
            hd_5,
            hd_6,
            hd_7,
            hd_8,
            hd_9,
            hd_10,
            hd_11,
            hd_12,
            hd_13,
            hd_14,
            hd_15,
            hd_16,
            hd_17,
            hd_18,
            hd_19
        FROM {db_name}.mdc_hierarchy_level_item_{project_id}""".format(**locals())
        execute_query(impala_conn, insert_query)
        log(log_path, "the query " + insert_query + " return true")
    except:
        log(log_path, "the query " + insert_query + " return false with the error: " + str(sys.exc_info()))

# rename the old table to the new table
def rename(db_name,old_name,new_name):
    try:
        rename_query = """ALTER TABLE {db_name}.{old_name} RENAME TO {db_name}.{new_name}""".format(**locals())
        execute_query(impala_conn, rename_query)
        log(log_path, "The query " + rename_query + " return true")
    except:
        log(log_path, "The query " + rename_query + " return false")

# drop the orginal (old) table
def drop_table(db_name,project_id):
    try:
        drop_query = """DROP TABLE {db_name}.mdc_hierarchy_level_item_{project_id}_old""".format(**locals())
        execute_query(impala_conn, drop_query)
        log(log_path, "The query " + drop_query + " return true")
    except:
        log(log_path, "The query " + drop_query + " return false")

# invalidate metadata on the new table
def invalidate_metadata(db_name,project_id):
    try:
        invalidate_metadata_query = """INVALIDATE METADATA {db_name}.mdc_hierarchy_level_item_{project_id}""".format(**locals())
        execute_query(impala_conn, invalidate_metadata_query)
        log(log_path, "The query " + invalidate_metadata_query + " return true")
    except:
        log(log_path, "The query " + invalidate_metadata_query + " return false")

# compute stats on the new table
def compute_stats(db_name,project_id):
    try:
        compute_stats_query = """COMPUTE STATS {db_name}.mdc_hierarchy_level_item_{project_id}""".format(**locals())
        execute_query(impala_conn, compute_stats_query)
        log(log_path, "The query " + compute_stats_query + " return true")
    except:
        log(log_path, "The query " + compute_stats_query + " return false")

def run_script():
    try:
        conn = pyodbc.connect(impala_conn, autocommit=True)
        log(log_path, "Successfully connect to the " + impala_conn + " connection")
        curs = conn.cursor()
        curs.execute("SHOW DATABASES")
        curs.commit()
        databases = curs.fetchall()
        db_names = [database[0] for database in databases]
        log(log_path, "SHOW DATABASES return " + ','.join(db_names))
        for db_name in db_names:
            tables = execute_query(impala_conn, "SHOW TABLES IN " + db_name + " LIKE '*mdc_hierarchy_level_item_*'")
            tbl_names = [table[0] for table in tables]
            log(log_path, "SHOW TABLES IN " + db_name + " LIKE '*mdc_hierarchy_level_item_*' return " + ','.join(db_names))
            if tbl_names:
                for tbl_name in tbl_names:
                    pro_id = tbl_name.split('_')[-1]
                    if pro_id.isdigit():
                        partition(db_name, pro_id)
                        insert(db_name, pro_id)
                        original_name = "mdc_hierarchy_level_item_" + str(pro_id)
                        old_name = "mdc_hierarchy_level_item_" + str(pro_id) + "_old"
                        rename(db_name, original_name, old_name)
                        old_name = "mdc_hierarchy_level_item_" + str(pro_id) + "_temp"
                        rename(db_name, old_name, original_name)
                        drop_table(db_name, pro_id)
                        invalidate_metadata(db_name, pro_id)
                        compute_stats(db_name, pro_id)
                    else:
                        log(log_path, "The project id accept for database " + db_name + " is not number")
            else:
                log(log_path, "Not exist tables that contains mdc_hierarchy_level_item_ for database " + db_name)
        return 1
    except:
        return 0

if __name__ == '__main__':
    run_script()